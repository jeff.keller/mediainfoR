# mediainfoR 0.2.3
* Add macOS support
* Minor bug fixes

# mediainfoR 0.2.2
* More informative error when MediaInfo CLI is not installed

# mediainfoR 0.2.1
* Added Windows support
* Fixed bug in mediaInfoVersion when the binary path contained spaces

# mediainfoR 0.2.0
* First functional release
