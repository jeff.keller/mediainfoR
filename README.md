# mediainfoR

An R wrapper for [mediainfo](https://mediaarea.net/en/MediaInfo) -- a convenient unified display of the most relevant technical and tag data for video and audio files.

# Install `mediainfoR`

First, install the MediaInfo CLI (see section below). Once you have the CLI installed, you can install this package with:

```
devtools::install_gitlab("jeff.keller/mediainfoR")
library(mediainfoR)
?mediainfoR
```

# Install MediaInfo CLI

You can find the latest instructions for your platform on the [MediaInfo Downloads page](https://mediaarea.net/en/MediaInfo/Download). Ensure you install the CLI tool. There is a GUI version of MediaInfo available that you may also be interested in, but the `mediainfoR` package requires at least the CLI tool be installed.

## Windows

1. Download the MediaInfo CLI tool from [here](https://mediaarea.net/en/MediaInfo/Download/Windows).
2. Extract the ZIP file (recommended location is `C:/Program Files/MediaInfo`)
3. Set the `mediainfo.path` option if you extracted the tool to another location, e.g.:

    ```
    library(mediainfoR)
    options(mediainfo.path = "C:/Users/john.smith/MediaInfo/MediaInfo.exe")
    ```

## Mac

1. Download and install the MediaInfo CLI tool from [here](https://mediaarea.net/en/MediaInfo/Download/Mac_OS).

## Linux

### Debian

```
apt-get install mediainfo
```

### Ubuntu/Ubuntu Derivatives

```
apt install mediainfo
```

### RedHat/CentOS

```
yum install mediainfo
```

### Fedora

```
yum install mediainfo
```

### SUSE/openSUSE

```
zypper install mediainfo
```

### Arch

```
pacman -S mediainfo
```
